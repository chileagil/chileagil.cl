
var gulp = require('gulp')
var connect = require('gulp-connect')
var less = require('gulp-less')
var sourcemaps = require('gulp-sourcemaps')

gulp.task('default', ['build'])
gulp.task('build', ['_build-less', '_build-html', '_build-assets', '_build-scripts'])

gulp.task('watch', ['build'], function () {
  connect.server({
    root: 'public',
    livereload: true
  })
  gulp.watch(['*.html'], ['_build-html'])
  gulp.watch(['assets/**'], ['_build-assets'])
  gulp.watch(['scripts/**/*.js'], ['_build-scripts'])
  gulp.watch(['styles/*.less'], ['_build-less'])
})

gulp.task('_build-html', [], function () {
  return gulp
    .src([
      'index.html'
    ], {base: '.'})
    .pipe(gulp.dest('public'))
    .pipe(connect.reload())
})

gulp.task('_build-assets', ['_copy-root-assets'], function () {
  return gulp
    .src([
      'assets/**/*', '!assets/root', '!assets/root/**/*'
    ], {base: '.'})
    .pipe(gulp.dest('public'))
    .pipe(connect.reload())
})

gulp.task('_copy-root-assets', [], function () {
  return gulp
  .src([
    'assets/root/*.*'
  ])
  .pipe(gulp.dest('public'))
  .pipe(connect.reload())
})

gulp.task('_build-scripts', [], function () {
  return gulp
    .src([
      'scripts/**/*.js'
    ], {base: '.'})
    .pipe(gulp.dest('public'))
    .pipe(connect.reload())
})

gulp.task('_build-less', [], function () {
  return gulp
    .src(['styles/*.less'])
    .pipe(sourcemaps.init())
    .pipe(less())
    .pipe(sourcemaps.write('./'))
    .pipe(gulp.dest('public/styles'))
    .pipe(connect.reload())
})
